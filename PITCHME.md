# W02 in beeld.

---

## Storytelling with data.

![IMAGE](assets/img/presentation.png)

---?color=linear-gradient(180deg, white 75%, black 25%)
@title[Inspiratie]

@snap[west span-50]
## Inspiratie 

@snapend

@snap[east span-50]
![IMAGE](assets/img/presentation1.png)
@snapend

@snap[south span-100 text-white]
oorlogsverleden storytel/map tool 

prototype, (2x)documentatie + productfilmpje
@snapend

---?color=linear-gradient(90deg, #5384AD 65%, white 35%)
@title[Afstuderen]

@snap[north-west h2-white]
#### Planning
@snapend

@snap[west span-55]
@ul[list-spaced-bullets text-white text-09]
- half november voortgangsgesprek 
- 21 januari coach
- *stage tot 2 februari
- 4 februari 20 weken afstuderen
@ulend
@snapend

@snap[east span-45]
@snapend
